package com.history.databaseAccess;

import com.history.databaseData.WaterConsumption;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface WaterConsumptionRepository extends MongoRepository<WaterConsumption, String> {

    public List<WaterConsumption> findByHouse(int house);
    public Long deleteWaterConsumptionByDateAndHouse(String date, int house);
    public void deleteAllByIdNotNull();
}
