package com.history.databaseData;

public class WaterConsumption {

    public void setId(int id) {
        this.id = id;
    }

    private int id;


    private int house;
    private String date;
    private int value;

    public WaterConsumption(int house, String date, int value){
        this.house = house;
        this.date = date;
        this.value = value;
    }

    public int getHouse() {
        return house;
    }
    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }


    public int getValue() {
        return value;
    }
}
