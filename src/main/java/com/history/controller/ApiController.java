package com.history.controller;

import com.history.HistoryService;
import com.history.databaseAccess.*;
import com.history.databaseData.*;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpGet;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpPost;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.classic.methods.HttpUriRequest;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.client5.http.impl.classic.HttpClients;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.HttpResponse;
import com.github.dockerjava.zerodep.shaded.org.apache.hc.core5.http.io.entity.StringEntity;
import com.history.databaseAccess.*;
import com.history.databaseData.*;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
public class ApiController {
    @Autowired
    private WaterConsumptionRepository WaterConsRepository;
    @Autowired
    private ElecConsumptionRepository ElecConsRepository;
    @Autowired
    private GasConsumptionRepository GasConsRepository;
    @Autowired
    private HouseInformationRepository HouseRepository;
    @Autowired
    private BuildModifRepository BuildRepository;


    public static boolean authorization(int id) throws IOException {
        /*HttpUriRequest request = new HttpGet( "https://localhost:8080/api/authorizations/"+id);

        HttpResponse response = HttpClientBuilder.create().build().execute(request);

        if(response.getCode() == 200){
            log.info("Access Granted");
            return true;
        }
        else{
            log.info("No Access. The access will be create");
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("https://localhost:8080/api/authorizations");

            String json = "{'id':0,'userId':'"+id+"', 'serviceId':'0', 'authorizationLevel':'0'}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response2 = client.execute(httpPost);
            return true;
        }*/
        return true;
    }

    @GetMapping("/water/{date}/{house}")
    @ApiOperation(value = "Water Get", notes = "Search Water Consumption")
    ResponseEntity<WaterConsumption> getWater(@PathVariable String date, @PathVariable int house) throws IOException {
        if (ApiController.authorization(house)) {
            List<WaterConsumption> results = WaterConsRepository.findByHouse(house);
            for (int i = 0; i < results.size(); i++) {
                if (date.equals(results.get(i).getDate())) {
                    log.info("Water consumption for house " + house + " at the date " + date + " is " + results.get(i).getValue());
                    return new ResponseEntity<>(results.get(i), HttpStatus.FOUND);
                }
            }
            log.info("Water consumption for house " + house + " at the date " + date + " is not found");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/elec/{date}/{house}")
    @ApiOperation(value = "Electricity Get", notes = "Search Electricity Consumption")
    ResponseEntity<ElecConsumption> getElec(@PathVariable String date, @PathVariable int house) throws IOException {
        if (ApiController.authorization(house)) {
            List<ElecConsumption> results = ElecConsRepository.findByHouse(house);
            for (int i = 0; i < results.size(); i++) {
                if (date.equals(results.get(i).getDate())) {
                    log.info("Electricity consumption for house " + house + " at the date " + date + " is " + results.get(i).getValue());
                    return new ResponseEntity<>(results.get(i), HttpStatus.FOUND);
                }
            }
            log.info("Electricity consumption for house " + house + " at the date " + date + " is not found");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }

    }

    @GetMapping("/gas/{date}/{house}")
    @ApiOperation(value = "Gas Get", notes = "Search Gas Consumption")
    ResponseEntity<GasConsumption> getGas(@PathVariable String date, @PathVariable int house) throws IOException {
        if (ApiController.authorization(house)) {
            List<GasConsumption> results = GasConsRepository.findByHouse(house);
            for (int i = 0; i < results.size(); i++) {
                if (date.equals(results.get(i).getDate())) {
                    log.info("Gas consumption for house " + house + " at the date " + date + " is " + results.get(i).getValue());
                    return new ResponseEntity<>(results.get(i), HttpStatus.FOUND);
                }
            }
            log.info("Gas consumption for house " + house + " at the date " + date + " is not found");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/house/{house}")
    @ApiOperation(value = "House Information Get", notes = "Search House Information")
    ResponseEntity<HouseInformation> getHouseInformations(@PathVariable int house) throws IOException {
        if (ApiController.authorization(house)) {
            HouseInformation houseinfo = HouseRepository.findByHouse(house);
            if (houseinfo != null) {
                log.info("Information for house number " + house + "is found");
                return new ResponseEntity<>(houseinfo, HttpStatus.FOUND);
            }
            log.info("Information for house number " + house + "is not found");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/buildmodif/{date}/{house}")
    @ApiOperation(value = "Build Modification Get", notes = "Search Build Modification")
    ResponseEntity<BuildModif> getModInformation(@PathVariable String date, @PathVariable int house) throws IOException {
        if (ApiController.authorization(house)) {
            List<BuildModif> results = BuildRepository.findByHouse(house);
            for (int i = 0; i < results.size(); i++) {
                if (date.equals(results.get(i).getDate())) {
                    log.info("Building modification for house " + house + " at the date " + date + " is found");
                    return new ResponseEntity<>(results.get(i), HttpStatus.FOUND);
                }
            }
            log.info("Building modification for house " + house + " at the date " + date + " is not found");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }


    @PostMapping("/water")
    @ApiOperation(value = "Water Post", notes = "Add a water consumption")
    ResponseEntity<WaterConsumption> addWater(@RequestBody WaterConsumption consum) throws IOException {
        if (ApiController.authorization(consum.getHouse())) {
            consum.setId(Integer.parseInt(consum.getDate()) + consum.getValue() + consum.getHouse());
            log.info("Water consumption is created in the database");
            return new ResponseEntity<>(WaterConsRepository.save(consum), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/elec")
    @ApiOperation(value = "Electricity Post", notes = "Add an electricity consumption")
    ResponseEntity<ElecConsumption> addElec(@RequestBody ElecConsumption consum) throws IOException {
        if (ApiController.authorization(consum.getHouse())) {
            consum.setId(Integer.parseInt(consum.getDate()) + consum.getValue() + consum.getHouse());
            log.info("Electricity consumption is created in the database");
            return new ResponseEntity<>(ElecConsRepository.save(consum), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/gas")
    @ApiOperation(value = "Gas Post", notes = "Add a gas consumption")
    ResponseEntity<GasConsumption> addGas(@RequestBody GasConsumption consum) throws IOException {
        if (ApiController.authorization(consum.getHouse())) {
            consum.setId(Integer.parseInt(consum.getDate()) + consum.getValue() + consum.getHouse());
            log.info("Gas consumption is created in the database");
            return new ResponseEntity<>(GasConsRepository.save(consum), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/house")
    @ApiOperation(value = "House Information Post", notes = "Add an house information")
    ResponseEntity<HouseInformation> addHouse(@RequestBody HouseInformation info) throws IOException {
        if (ApiController.authorization(info.getHouse())) {
            info.setId(info.getHouse() + info.getBathrooms() + info.getConstructionYear() + info.getRooms());
            log.info("Information for house number " + info.getHouse() + " is created in the database");
            return new ResponseEntity<>(HouseRepository.save(info), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/buildmodif")
    @ApiOperation(value = "Build Modification Post", notes = "Add a build modification")
    ResponseEntity<BuildModif> addBuildModif(@RequestBody BuildModif info) throws IOException {
        if (ApiController.authorization(info.getHouse())) {
            info.setId(info.getHouse() + Integer.parseInt(info.getDate()));
            log.info("Building modification is created in the database");
            return new ResponseEntity<>(BuildRepository.save(info), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping("/water/{date}/{house}")
    @ApiOperation(value = "Water Delete", notes = "Delete a water consumption")
    ResponseEntity<String> delWater(@PathVariable String date, @PathVariable int house) throws IOException {
        if (house == 1000) {
            log.info("All Water consumption collection deleted");
            WaterConsRepository.deleteAllByIdNotNull();
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        if (ApiController.authorization(house)) {
            if (WaterConsRepository.deleteWaterConsumptionByDateAndHouse(date, house) > 0) {
                log.info("Water consumption for house " + house + " at the date " + date + " is deleted");
                return new ResponseEntity<>("Deleted", HttpStatus.OK);
            } else {
                log.info("Water consumption for house " + house + " at the date " + date + " is not found");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping("/elec/{date}/{house}")
    @ApiOperation(value = "Electricity Delete", notes = "Delete an electricity consumption")
    ResponseEntity<String> delElec(@PathVariable String date, @PathVariable int house) throws IOException {
        if (house == 1000) {
            log.info("All Electricity consumption collection deleted");
            ElecConsRepository.deleteAllByIdNotNull();
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        if (ApiController.authorization(house)) {
            if (ElecConsRepository.deleteElecConsumptionByDateAndHouse(date, house) > 0) {
                log.info("Electricity consumption for house " + house + " at the date " + date + " is deleted");
                return new ResponseEntity<>("Deleted", HttpStatus.OK);
            } else {
                log.info("Electricity consumption for house " + house + " at the d2ate " + date + " is not found");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping("/gas/{date}/{house}")
    @ApiOperation(value = "Gas Delete", notes = "Delete a gas consumption")
    ResponseEntity<String> delGas(@PathVariable String date, @PathVariable int house) throws IOException {
        if (house == 1000) {
            log.info("All Gas consumption collection deleted");
            GasConsRepository.deleteAllByIdNotNull();
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        if (ApiController.authorization(house)) {
            if (GasConsRepository.deleteGasConsumptionByDateAndHouse(date, house) > 0) {
                log.info("Gas consumption for house " + house + " at the date " + date + " is deleted");
                return new ResponseEntity<>("Deleted", HttpStatus.OK);
            } else {
                log.info("Gas consumption for house " + house + " at the date " + date + " is not found");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping("/house/{house}")
    @ApiOperation(value = "House Information Delete", notes = "Delete an house information")
    ResponseEntity<String> delHouse(@PathVariable int house) throws IOException {
        if (house == 1000) {
            log.info("All house information collection deleted");
            HouseRepository.deleteAllByIdNotNull();
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        if (ApiController.authorization(house)) {
            if (HouseRepository.deleteHouseInformationByHouse(house) > 0) {
                log.info("Informations from house " + house + " are deleted");
                return new ResponseEntity<>("Deleted", HttpStatus.OK);
            } else {
                log.info("Informations from house " + house + " are not found");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping("/buildmodif/{date}/{house}")
    @ApiOperation(value = "Build Modification Delete", notes = "Delete a build modification")
    ResponseEntity<String> delBuildModif(@PathVariable String date, @PathVariable int house) throws IOException {
        if (house == 1000) {
            log.info("All building modification collection deleted");
            BuildRepository.deleteAllByIdNotNull();
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        if (ApiController.authorization(house)) {
            if (BuildRepository.deleteBuildModifByDateAndHouse(date, house) > 0) {
                log.info("Building modification for house " + house + " at the date " + date + " is deleted");
                return new ResponseEntity<>("Deleted", HttpStatus.OK);
            } else {
                log.info("Building modification for house " + house + " at the date " + date + " is not found");
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        }
    }
}
