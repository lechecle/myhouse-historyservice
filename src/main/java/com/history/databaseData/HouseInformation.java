package com.history.databaseData;

public class HouseInformation {

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    private int house;
    private int rooms;
    private int constructionYear;
    private int bathrooms;

    public HouseInformation(int rooms, int constructionYear, int bathrooms, int house) {
        this.rooms = rooms;
        this.constructionYear = constructionYear;
        this.bathrooms = bathrooms;
        this.house = house;
    }

    public int getHouse() {
        return house;
    }

    public int getRooms() {
        return rooms;
    }

    public int getConstructionYear() {
        return constructionYear;
    }

    public int getBathrooms() {
        return bathrooms;
    }


}
