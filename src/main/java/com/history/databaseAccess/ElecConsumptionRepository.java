package com.history.databaseAccess;

import com.history.databaseData.ElecConsumption;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ElecConsumptionRepository extends MongoRepository<ElecConsumption, String> {

    public List<ElecConsumption> findByHouse(int house);
    public Long deleteElecConsumptionByDateAndHouse(String date, int house);
    public void deleteAllByIdNotNull();
}
