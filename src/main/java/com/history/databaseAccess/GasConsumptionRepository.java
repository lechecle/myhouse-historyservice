package com.history.databaseAccess;

import com.history.databaseData.GasConsumption;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface GasConsumptionRepository extends MongoRepository<GasConsumption, String> {

    public List<GasConsumption> findByHouse(int house);
    public Long deleteGasConsumptionByDateAndHouse(String date, int house);
    public void deleteAllByIdNotNull();
}
