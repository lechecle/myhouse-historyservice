package com.history.databaseAccess;

import com.history.databaseData.HouseInformation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HouseInformationRepository extends MongoRepository<HouseInformation, String> {
    public HouseInformation findByHouse(int house);
    public Long deleteHouseInformationByHouse(int house);
    public void deleteAllByIdNotNull();
}
