package com.history.databaseData;

public class BuildModif {

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    private int house;
    private String date;
    private String description;
    private String company;


    public String getCompany() {
        return company;
    }

    public BuildModif(String date, String description, int house, String company){
        this.description = description;
        this.house = house;
        this.date = date;
        this.company = company;

    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }
    public int getHouse() {
        return house;
    }
}
