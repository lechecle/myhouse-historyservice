package com.history.databaseAccess;

import com.history.databaseData.BuildModif;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BuildModifRepository extends MongoRepository<BuildModif, String> {
    public List<BuildModif> findByHouse(int house);
    public Long deleteBuildModifByDateAndHouse(String date, int house);
    public void deleteAllByIdNotNull();
}
