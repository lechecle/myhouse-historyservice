package com.history;

import com.history.databaseData.BuildModif;
import com.history.databaseData.HouseInformation;
import com.history.databaseData.WaterConsumption;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HistoryServiceTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void newClientTest(){
		HouseInformation house = new HouseInformation(2,1952,1,1);
		HouseInformation houseInfo = this.restTemplate.postForObject("/house", house, HouseInformation.class);
		assertThat(houseInfo).isEqualToIgnoringGivenFields(house,"id");

		int[] consos = new int[12];
		Random rdn = new Random();

		String[] date = {"0120","0220","0320","0420","0520","0620","0720","0820","0920","1020","1120","1220"};
		for(int i=0; i<12; i++){
			consos[i] = rdn.nextInt(100);
			WaterConsumption conso = new WaterConsumption(1, date[i],consos[i]);
			WaterConsumption consoCreated = this.restTemplate.postForObject("/water", conso, WaterConsumption.class);
			assertThat(consoCreated).isEqualToIgnoringGivenFields(conso, "id");


		}
		WaterConsumption consoRequest = this.restTemplate.getForObject("/water/{date}/{house}", WaterConsumption.class, "0320",1);
		assertThat(consoRequest).isEqualToIgnoringGivenFields(new WaterConsumption(1,"0320",consos[2]),"id");

		WaterConsumption consoRequest2 = this.restTemplate.getForObject("/water/{date}/{house}", WaterConsumption.class, "0620",1);
		assertThat(consoRequest2).isEqualToIgnoringGivenFields(new WaterConsumption(1,"0620",consos[5]),"id");

		WaterConsumption consoRequest3 = this.restTemplate.getForObject("/water/{date}/{house}", WaterConsumption.class, "0628",1);
		assertThat(consoRequest3).isEqualTo(null);

		BuildModif modif = new BuildModif("0421", "works in the living room", 1, "PrahaWork");
		BuildModif modifrequest = this.restTemplate.postForObject("/buildmodif", modif, BuildModif.class);
		assertThat(modif).isEqualToIgnoringGivenFields(modifrequest,"id");

		BuildModif getTest = this.restTemplate.getForObject("/buildmodif/{date}/{house}", BuildModif.class, "0421", 1);
		assertThat(getTest).isEqualToIgnoringGivenFields(modif,"id");

		BuildModif getTest2 = this.restTemplate.getForObject("/buildmodif/{date}/{house}", BuildModif.class, "0421", 2);
		assertThat(getTest2).isEqualTo(null);

		this.restTemplate.delete("/water/{date}/{house}",  "0320",1);
		WaterConsumption delRequest = this.restTemplate.getForObject("/water/{date}/{house}", WaterConsumption.class, "0320",1);
		assertThat(delRequest).isEqualTo(null);

		this.restTemplate.delete("/water/{date}/{house}",  "0320",1000);
		this.restTemplate.delete("/elec/{date}/{house}",  "0320",1000);
		this.restTemplate.delete("/gas/{date}/{house}",  "0320",1000);
		this.restTemplate.delete("/house/{house}",1000);
		this.restTemplate.delete("/buildmodif/{date}/{house}",  "0320",1000);
	}

}
